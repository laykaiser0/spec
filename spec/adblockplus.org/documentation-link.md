Documentation link (/redirect)
========================

The Documentation link page is used by the our adblocking products to redirect the user to a url specified by a parameter. This way we can update the url without having to update the product.

**Url**:

```
/redirect
```

|Parameter|Description|
|---------|-----------|
|link|the name of the link (refer to the table below for valid link names)|
|lang|the language the url should be displayed in if possible (defaults to en if the target is not available in requested language or the language has been ommitted)|



|Name|Locale[1]|GeoIp[2]|target url|
|----|------|-----|----------|
|reporter_connect_issue|-|-|`/forum/`|
|reporter_other_link|-|-|`/forum/`|
|firefox_support|-|-|`/forum/viewforum.php?f=1`|
|chrome_support|-|-|`/forum/viewforum.php?f=10`|
|opera_support|-|-|`/forum/viewforum.php?f=14`|
|safari_support|-|-|`/forum/viewforum.php?f=18`|
|edge_support|-|-|`/forum/viewforum.php?f=27`|
|knownIssuesChrome_filterstorage|-|-|`/forum/viewtopic.php?t=23597`|
|acceptable_ads_opt_out|-|-|`https://adblockplus.org/$lang/acceptable-ads#optout`|
|`^adblock_browser_promotion_\d$`|-|-|`https://adblockbrowser.org/?link=adblock_browser_promotion`|
|adblock_browser_android_support|-|-|`/forum/viewforum.php?f=24`|
|adblock_browser_android_faq|-|-|`/forum/viewforum.php?f=24`|
|adblock_browser_ios_support|-|-|`/forum/viewforum.php?f=25`|
|adblock_browser_android_store|-|`*`|`https://play.google.com/store/apps/details?id=org.adblockplus.browser`|
|adblock_browser_android_store|-|`cn`|`https://downloads.adblockplus.org/adblockbrowser-1.1.0-arm.apk`|
|adblock_browser_ios_store|-|-|`https://geo.itunes.apple.com/us/app/adblock-browser-best-ad-blocker/id1015653330?mt=8`|
|adblock_browser_android_download|-|-|`https://downloads.adblockplus.org/adblockbrowser-1.1.0-arm.apk`|
|adblock_browser_website|-|-|`https://adblockbrowser.org/`|
|adblock_plus_safari_ios_support|-|-|`/forum/viewforum.php?f=26`|
|adblock_plus_safari_ios_store|-|-|`https://itunes.apple.com/app/adblock-plus-abp/id1028871868`|
|adblock_plus_sbrowser_store|-|-|`https://play.google.com/store/apps/details?id=org.adblockplus.adblockplussbrowser`|
|adblock_plus_chrome_dnt|-|-|`https://support.google.com/chrome/answer/2790761`|
|adblock_plus_firefox_dnt|-|-|`https://www.mozilla.org/en-US/firefox/dnt/`|
|adblock_plus_opera_dnt|-|-|`https://help.opera.com/en/latest/security-and-privacy/#tracking`|
|adblock_plus_edge_dnt|-|-|`https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy`|
|adblock_plus_report_bug|-|-|`https://adblockplus.org/bugs#reporting`|
|adblock_plus_report_ad|-|-|`https://forums.lanik.us/viewforum.php?f=62`|
|adblock_plus_report_issue|`en`|-|`https://forums.lanik.us/viewforum.php?f=64`|
|adblock_plus_report_issue|`id`|-|`https://forums.lanik.us/viewforum.php?f=94`|
|adblock_plus_report_issue|`nl`|-|`https://forums.lanik.us/viewforum.php?f=100`|
|adblock_plus_report_issue|`de`|-|`https://forums.lanik.us/viewforum.php?f=90`|
|adblock_plus_report_issue|`it`|-|`https://forums.lanik.us/viewforum.php?f=96`|
|adblock_plus_report_issue|`es`|-|`https://forums.lanik.us/viewforum.php?f=103`|
|adblock_plus_report_issue|`lt`|-|`https://forums.lanik.us/viewforum.php?f=101`|
|adblock_plus_report_issue|`lv`|-|`https://forums.lanik.us/viewforum.php?f=99`|
|adblock_plus_report_issue|`ar`|-|`https://forums.lanik.us/viewforum.php?f=98`|
|adblock_plus_report_issue|`fr`|-|`https://forums.lanik.us/viewforum.php?f=91`|
|adblock_plus_report_issue|`ru`|-|`https://forums.lanik.us/viewforum.php?f=102`|
|adblock_plus_report_issue|`*`|-|`https://forums.lanik.us/viewforum.php?f=64`|
|releases|-|-|`https://adblockplus.org/releases/`|
|social_facebook|-|-|`https://www.facebook.com/adblockplus`|
|social_twitter|-|-|`https://twitter.com/adblockplus`|
|social_weibo|-|-|`http://e.weibo.com/adblockplus/`|
|`^share-`|IF APL|-|`https://share.adblockplus.org/$lang/`|
|uninstalled|IF APL|-|`https://adblockplus.org/$lang/uninstalled?link=uninstalled`|
|gettingStarted|IF APL|-|`https://adblockplus.org/$lang/getting_started`|
|faq|IF APL|-|`https://adblockplus.org/$lang/faq`|
|filterdoc|-|-|`https://help.eyeo.com/adblockplus/how-to-write-filters`|
|subscriptions|IF APL|-|`https://adblockplus.org/$lang/subscriptions`|
|language_subscription|IF APL|-|`https://adblockplus.org/getting_started#subscription`|
|reporter_privacy|IF APL|-|`https://adblockplus.org/$lang/privacy#issue-reporter`|
|privacy|IF APL|-|`https://adblockplus.org/$lang/privacy`|
|contribute|IF APL|-|`https://adblockplus.org/$lang/contribute`|
|donate|IF APL|-|`https://adblockplus.org/$lang/donate`|
|donate_frp_page|IF APL|-|`https://adblockplus.org/donate?utm_source=abp&utm_medium=frp_page&utm_campaign=donate`|
|donate_settings_page|IF APL|-|`https://adblockplus.org/$lang/donate?utm_source=abp&utm_medium=settings_page&utm_campaign=donate`|
|donation_update_page|IF APL|-|`https://adblockplus.org/donate?utm_source=abp&utm_medium=update_page&utm_campaign=donate`|
|acceptable_ads|IF APL|-|`https://adblockplus.org/$lang/acceptable-ads`|
|acceptable_ads_criteria|IF APL|-|`https://adblockplus.org/$lang/acceptable-ads#criteria`|
|acceptable_ads_survey|IF APL|-|`https://adblockplus.org/acceptable-ads-survey`|
|privacy_friendly_ads|IF APL|-|`https://adblockplus.org/acceptable-ads#privacy-friendly-acceptable-ads`|
|contributors|IF APL|-|`https://adblockplus.org/$lang/contributors`|
|whitelist|IF APL|-|`https://adblockplus.org/$lang/faq_basics#disable`|
|adblock_plus|IF APL|-|`https://adblockplus.org/`|
|eyeo|-|-|`https://eyeo.com/`|
|developer|-|-|`https://adblockplus.org/forum/viewforum.php?f=4`|
|help_center|-|-|`http://help.eyeo.com/`|
|help_center_abp_en|-|-|`https://help.eyeo.com/en/adblockplus/`|
|imprint|IF APL|-|`https://adblockplus.org/$lang/imprint`|
|block_element|-|-|`https://help.eyeo.com/adblockplus/block-item-or-element`|
|chrome_review|-|-|`https://chrome.google.com/webstore/detail/adblock-plus-free-ad-bloc/cfhdojbkjhnklbpkdaibdccddilifddb/reviews`|
|firefox_review|-|-|`https://addons.mozilla.org/firefox/addon/adblock-plus/reviews`|
|opera_review|-|-|`https://addons.opera.com/extensions/details/adblock-plus/`|
|chrome_review_low|-|-|`https://chrome.google.com/webstore/detail/adblock-plus-free-ad-bloc/cfhdojbkjhnklbpkdaibdccddilifddb/reviews`|
|firefox_review_low|-|-|`https://addons.mozilla.org/firefox/addon/adblock-plus/`|
|opera_review_low|-|-|`https://addons.opera.com/extensions/details/adblock-plus/`|
|adware|-|-|`https://help.eyeo.com/adblockplus/adware`|
|adblock_plus_release_blog_3.7|-|-|`https://adblockplus.org/blog/new-release-adblock-plus-37`|
|adblock_plus_release_blog_3.8|-|-|`https://adblockplus.org/blog/new-release-adblock-plus-38`|
|chrome_store|-|-|`https://chrome.google.com/webstore/detail/adblock-plus-free-ad-bloc/cfhdojbkjhnklbpkdaibdccddilifddb`|
|firefox_store|-|-|`https://addons.mozilla.org/firefox/addon/adblock-plus/`|
|opera_store|-|-|`https://addons.opera.com/extensions/details/adblock-plus/`|
|edge_store|-|-|`https://microsoftedge.microsoft.com/addons/detail/gmgoamodcdcjnbaobigkjelfplakmdhh`|

[1] ***Locale***: *-* ignore locale, `*` apply for all unspecified locales, `xy` apply only for specified locale, *IF APL* (If applicable) redirect to locale specific document if applicable

[2] ***GeoIp***: *-*: ignore GeoIp, `*` apply for all unspecified GeoIp, `xy` apply only for specified GeoIp
