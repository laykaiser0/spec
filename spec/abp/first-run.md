# First Run Page

1. [Opening behavior](#opening-behavior)
1. [Navigation](#navigation)
1. [Content](#content)
1. [Warnings](#warnings)
1. [Assets](#assets)

## Opening behavior

The First Run Page will be shown following a successful installation of the extension. This page will open in a new tab and is a one-time event.

The extension will show the local version of the First Run Page, as described by this document, for German users, or if it has identified some data corruption. Otherwise, it will attempt to show the web-based version (see [URL resolution logic](#url-resolution)). If the web-based version fails to load, it will show the local version instead.

### URL resolution

For showing the web-based version, the URL `https://welcome.adblockplus.org/{language}/installed` should be used. The language provided in that URL should be a code representing either the language (e.g. `de`) or the exact locale (e.g. `de_AT`) of the extension UI.

If the page is not available for that exact locale, it should redirect to a generic version for that language (e.g. `/de_AT/installed` may redirect to `/de/installed`). If no generic version for the language is available either, it should redirect to the generic version of that page (e.g. `/de_AT/installed` may redirect to `/installed`).

In addition to the language, the URL should also contain the following information via query string parameters:

| Parameter | Description |
|-|-|
|`an`|Extension name|
|`av`|Extension version|
|`ap`|Browser name|
|`apv`|Browser version|
|`p`|Browser engine name|
|`pv`|Browser engine version|

## Navigation

### Header

* Logo - Redirects to the [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=adblock_plus`
* `DONATE` - Redirects to the [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=donate_frp_page`

### Footer

`Copyright © 2018 eyeo GmbH. All rights reserved. Adblock Plus® is a registered trademark of [eyeo GmbH][1].

[1] eyeo redirect to eyeo.com

## Content

Design: [https://eyeogmbh.invisionapp.com/public/share/MC1C7VJHVE](https://eyeogmbh.invisionapp.com/public/share/MC1C7VJHVE)


```
 # INSTALLATION SUCCESSFUL!

You just took control of your browser

 ## YOU'RE IN CONTROL

By default, you may see some nonintrusive ads that adhere to [strict criteria][0]. We identify these ads as Acceptable Ads. Prefer to block all ads? [Turn off Acceptable Ads][1] in your [Settings][2].

 ## ADBLOCK BROWSER APP

From the team behind Adblock Plus, the most popular ad blocker for desktop browsers, Adblock Browser is now available for your Android and iOS devices.

 ## FAIR USE

We encourage you to make fair use of Adblock Plus and the option to whitelist websites. Depending on the filters you use, you understand and accept that unintentional results might occur (e.g. blocked content on a website). Refer to the [Terms of Use][3] for more information about fair use of Adblock Plus.
```

[0]: Opens Documentation link: %LINK%=acceptable_ads_criteria in a new tab

[1]: Opens Documentation link: %LINK%=acceptable_ads_opt_out in a new tab

[2]: Opens: Settings menu in a new tab

[3]: Opens Documentation link: %LINK%=terms in a new tab

#### App store links

| App store | Link | alt text |
| --- | ---- | --- | 
| iTunes |  %LINK%=adblock_browser_ios_store | Available on the iTunes App Store |
| Google Play |  %LINK%=adblock_browser_android_store | Android app on Google Play |

#### Responsiveness

- Optimize for a screen widths of 1200px, 992px, 768px and 576px.
- Readable text area on large screens should not extend beyond 1024px in width.
- Spacing between buttons should become wider as the column width increases, and should stack as the space decreases.
- It is important that the text in the first column appears above the fold. 

#### Link behaviour 

All external links, including links to adblockplus.org should open in a new tab.


#### User support text

Text in smaller font size to not distract from the Explanation text that tells the user to contact user support.

`If you still need help, contact us at [support@adblockplus.org][0] or visit our [Help Center][1].`

\[0]: support@adblockplus.org

[1]: Opens Documentation link: %LINK%=help_center in a new tab.


## Warning

![](/res/abp/first-run/abp-first-run-page-error.png)

When an error message is shown, then hide the headline text `# INSTALLATION SUCCESSFUL! You just took control of your browser`.

If there are more than one warning messages - only display the [data corruption warning](#data-corruption-warning).

### Filter list warning

`It seems that an issue caused all filters to be removed and we were unable to restore a backup. Therefore we had to reset your filters and Acceptable Ads settings. Please check your filter lists and Acceptable Ads settings in the [Adblock Plus options](Open Settings Page in a new tab).`

### Data corruption warning

If the first run page will be shown and the data corruption flag is set show the below text.

#### Explanation text

Explanation text that informs the user that his settings storage has been corrupted and that he needs to reinstall the extension.

```
Some users are experiencing an issue where this page opens each time their browser is launched. This issue can be resolved by uninstalling and reinstalling Adblock Plus:

1. Open your browser's Extensions tab.
2. Locate Adblock Plus and remove it.
3. Reinstall Adblock Plus from [adblockplus.org][0].
```

[0]: Opens Documentation link: %LINK%=adblock_plus in a new tab.


## Assets

| Name | File |
| ---- | ---- |
| abp-full-logo.svg | ![](/res/abp/first-run/assets/abp-full-logo.svg) |
| checkmark-header.svg | ![](/res/abp/first-run/assets/checkmark-header.svg) |
| checkmark.svg | ![](/res/abp/first-run/assets/checkmark.svg) |
| rocket.svg | ![](/res/abp/first-run/assets/rocket.svg) |
| lock.svg | ![](/res/abp/first-run/assets/lock.svg) |
| googleplay-bg.svg | ![](/res/abp/first-run/assets/googleplay-bg.svg) |
| appstore-bg.svg | ![](/res/abp/first-run/assets/appstore-bg.svg) |
| footer-facebook-glyphicon.png | ![](/res/abp/first-run/assets/footer-facebook-glyphicon.png) |
| footer-instagram-glyphicon.png | ![](/res/abp/first-run/assets/footer-instagram-glyphicon.png) |
| footer-twitter-glyphicon.png | ![](/res/abp/first-run/assets/footer-twitter-glyphicon.png) |
| footer-youtube-glyphicon.png | ![](/res/abp/first-run/assets/footer-youtube-glyphicon.png) |
