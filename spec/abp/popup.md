# Adblock Plus: Bubble UI

The bubble UI is the menu which is triggered when the Adblock Plus icon in the browser toolbar is clicked. The bubble UI displays notifications, top-level information on the extension usage and provides access to the extension's settings.

1. [General requirements](#general-requirements) 
1. [Index](#index) 
1. [Toolbar icon](#toolbar-icon)
1. [Notifications](#notifications)
1. [Style guide](#style-guide) 
1. [Assets](#assets)

## General requirements

- Width: 340px

## Index

**General Design:** https://eyeogmbh.invisionapp.com/share/Q3THNN576PX#/screens/379344336
 
1. Logo [image](/res/abp/adblockplus-colour-web.svg)
1. Gear icon - Launches [Settings page](/spec/abp/desktop-settings.md) in a new tab (Refer to the [Style guide](#style-guide) for hover/click states)
1. [Toggle](#toggle)
1. `Number of items blocked`
1. [Counter panel](#counter-panel)
1. [Report issue](#report-issue) 
1. [Block element](#block-element)
1. `Want ad blocking on your mobile device? [1] [2]` 

[1] adblock_browser_ios_store 

[2] adblock_browser_android_store

## Toggle

![](/res/abp/bubble-ui/ABP-Enabled-Disabled.png) 

The toggles to enable/disable the extension are only active on HTTP(S) sites, otherwise they are [inactive](#inactive) (i.e. an extension page).

### Enable/Disable behavior:

- Disabling the toggle on domain level also disables the toggle on page level.
- When the toggle on domain level is disabled, the page level toggle can not be enabled.
- Enabling the toggle on domain level also enables the toggle on page level.

### Enable/Disable text:
`Block ads on:` 

`This website:`
`<exampledomain.com>`

`This page:` 
`</examplepath>`

- Long domains are truncated with '...' at the end
- Only URL's path is shown for page level
  - URL's with more than one parameter show '...' at the end in place of parameter details: `example.com/test?…`

### Refresh

**Enable/Disable Design:** 
https://eyeogmbh.invisionapp.com/share/TYSWLRTQ37D#/

- After enabling or disabling any of the toggles, the page needs to be reloaded for the setting to take effect.
- Refresh message is shown when toggles' state changes
  - Title: `Refresh this page`
  - Description: `Click the button below for changes to take effect.`
  - Button: `Refresh`
- Clicking `Refresh` reloads the webpage and closes the bubble UI.
- If changes to toggles are reverted before the page is reloaded, the refresh message disappears

# Inactive

**Design:** https://eyeogmbh.invisionapp.com/share/A4TRD7REVFZ#/screens/382227291

The extension is inactive on non HTTP(S) sites.

Both the toggle panel and ad counter for "this page" are hidden.

## Counter panel

**Design:** https://eyeogmbh.invisionapp.com/share/A4TRD7REVFZ

**Assets:** [/res/abp/bubble-ui/assets/social-sharing/](/res/abp/bubble-ui/assets/social-sharing/)

**Section title:** Number of items blocked

- `<#> on this page`: `#` shows the number of blocked items by the extension on the current page. The panel is hidden on whitelisted websites and pages. 
- `<#> in total` : `#` shows the total number of blocked items by the extension since it had been installed. 
- `Share numbers with friends` : clicking on this shows the Facebook and Twitter icons, that allow the user to share the **total** amount of blocked items.
  - `Cancel` reverts back to the `Share numbers with friends` message.
  - Facebook content:  
      - Add link to: `https://adblockplus.org`
      - If blocked number is < 1000 show hashtag: `#AdblockPlus100`
      - If blocked number is < 10000 show hashtag: `#AdblockPlus1000` 
      - If blocked number is < 100000 show hashtag: `#AdblockPlus10K`
      - If blocked number is < 1000000 show hashtag: `#AdblockPlus100K`
      - If blocked number is >= 10000000 show hashtag: `#AdblockPlus1M`
  - Twitter/Weibo content: `I've blocked XX ads with Adblock Plus. Give it a try - it's FREE! https://adblockplus.org/`, where `XX` uses the total number of blocked items.
  - Screen readers description:
    - Facebook icon - `Share on Facebook`
    - Twitter icon - `Share on Twitter`
    - Weibo icon - `Share on Weibo`
  - Only users with Chinese language settings have option to share also on Weibo

## Block element and Report issue 

**Block element and Report issue design:** https://eyeogmbh.invisionapp.com/share/4CTE0RUW2BK

**Hover design:** https://eyeogmbh.invisionapp.com/share/Y2TE1L6P9R8

**Assets:** [/res/abp/popup/assets/](/res/abp/popup/assets/)

Block element button CTA: `Block element`, subtext: `Block specific element on this website`

Report issue button CTA: `Report an issue on this page`

### Block element

**Design:** https://eyeogmbh.invisionapp.com/public/share/KQXAE13Y6H7#/screens/417664367

`Block element` only appears on webpages when the extension is active and when there are detectable elements, otherwise this feature is inactive. 

Process description:

- Click on 'Block element' 'Block specific element on this website' displays the message (in the Bubble UI) `Click an element on the page to block it.` This Bubble UI closes automatically after 5 seconds.
- [Design page 3](https://eyeogmbh.invisionapp.com/public/share/KQXAE13Y6H7#/screens/417664369) - Click `Cancel` to close the menu and return to the main bubble UI menu. 
- After user click on the element, display the block element dialog box from [compser.md](/spec/abp/composer.md)

### Report issue

This is only active when the extension is active on the page. Clicking this launches the [Issue Reporter](/spec/abp/issue-reporter.md) in a new tab. 

For translations that run long, then stack the items for [report issue](#report-issue) and [block element](#block-element) into a single column.

## Notifications 

| Informational notification | Critical notification |
| ---- | ---- |
| ![](/res/abp/popup/info-notification.png) | ![](/res/abp/popup/critical-notification.png) |

### All notifications (except critical notifications)

1. Clicking 'Close' hides the current message.
1. Clicking 'Stop showing notifications' hides the current notification, prevents further notifications from showing up (except critical ones), and unchecks the "Show useful notifications" option.

### Critical notification

Clicking `Close` hides the current message.

## Toolbar icon

| Icon state | Description |
| --- | --- |
| ![](/res/abp/popup/toolbar-icon.png) | Extension enabled |
| ![](/res/abp/popup/abp-whitelisted.png) | Extension disabled |
| ![](/res/abp/popup/toolbar-icon-count.png) | Number of elements blocked on the active tab |
| ![](/res/abp/popup/abp-notification-critical.png) | Critical alert notification |
| ![](/res/abp/popup/abp-notification-information.png) | Informational notification |

## Style guide

All clickable elements (i.e. gear icon, report issue, block element, cancel) should have the following states: 
1. **Active:** Dark grey
2. **Hover / On click / Selected:** Highlighted in blue
3. **Inactive:** (not always applicable) Light grey

## Assets 

| Name | File |
| --- | --- |
| iconClose.svg | ![](/res/abp/popup/assets/iconClose.svg) |
| iconOff.svg | ![](/res/abp/popup/assets/iconOff.svg) |
| iconOn.svg | ![](/res/abp/popup/assets/iconOn.svg) |
| options.svg | ![](/res/abp/popup/assets/options.svg) |
| iconAndroid.svg | ![](/res/abp/popup/assets/iconAndroid.svg) |
| iconApple.svg | ![](/res/abp/popup/assets/iconApple.svg) |
| iconInfo.svg | ![](/res/abp/popup/assets/iconInfo.svg) |
| iconCritical.svg | ![](/res/abp/popup/assets/iconCritical.svg) |

More assets: [/res/abp/popup/assets/](/res/abp/popup/assets/)





