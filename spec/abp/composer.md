# Filter composer

Filter composer dialog of the Adblock Plus browser extension.

The filter composer is used to hide a selected element or block requests that are related to the selected element.
Block element feature can be used in the following ways:
- Context menu `Block element` - Right-click on image, audio, video or picture in websites.
- Bubble UI

Design (screens 4-9): https://eyeogmbh.invisionapp.com/share/KQXAE13Y6H7#/screens/417664375

Dialog box title: `Adblock Plus`
- `element(s) selected` - Shows the number of selected items.
- `Unselect` - Unselects items and allows the user to select new items. Close the dialog box so user can pick new item. 
- `Preview` - Shows the page without the selected item(s). Disabled if suggested filters have been modified.
- `Exit preview` - Shows again the page with the selected item(s).
- `Block` - Adds the item(s) to the custom filter list. This button is focused by default and can also be triggered by pressing Enter. Disabled whenever [filter area](#filter-area) contains no filters.
    - If valid: Closes the dialog
    - If invalid: Shows the appropriate error message
- `Cancel` - Cancels block element process and closes the dialog box. Can also be triggered by pressing Escape.

## Filter area

User-editable text input field that contains filters that were suggested by the extension to match the selected element. Stretches to occupy any available space.

The area should be focused when opening the dialog. It is disabled and shows placeholder text `Loading...` until it gets populated with filters. It is also disabled whenever the preview is active.
