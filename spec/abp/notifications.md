# The Adblock Plus notification system

Adblock Plus has a system for showing various types of notifications to users under certain conditions.

- [What's missing from this document](#whats-missing-from-this-document)
- [Notification types](#notification-types)
- [Display methods](#display-methods)
- [Notification content](#notification-content)
- [Targeting options](#targeting-options)
- [Notification sources](#notification-sources)
- [Notification repository format](#notification-repository-format)
- [User journey](#user-journey)

## What's missing from this document

The notification system is quite extensive, a few things are still missing from this document:

**Notification categories** - There is a mechanism to group notifications to allow the user to opt-out of only notifications of the same category.

**Notification groups** - There is a mechanism for having notifications only appear for a random subset of the user base.

## Notification types

The notification type indicates which kind of notification should be shown and thereby how it should be displayed and behave.

| Severity | Type | [Persistent](#persistence) | Affected by [opt-out](#opt-out) |
|-|-|-|-|
|2|critical|yes|no|
|1|relentless|no|no|
|1|newtab|no|yes|
|0|information|no|yes|
|-|normal|no|yes|
|0|(default)|no|yes|

### Severity

ABP never shows the user more than one notification at a time. If multiple notifications are supposed to be shown, the notification with the highest _severity_ is shown first. If multiple notifications have the same _severity_, the first notification in the list is shown first.

After the first notification has been closed, the next notification will be shown the next time its condition is met (i.e. after the next update check). Note that critical notifications will show up each time their condition is met, and will therefore prevent all other notifications from being shown.

### Persistence

Any non-persistent notifications will be shown only once unless an [interval](#interval) is specified. Any non-persistent notification (excl. [data corruption notification](#warning-data-corruption-and-filters-reset)) will not be shown, in case of a data corruption. Persistent notifications will be shown each time a [notification check](#update-check-frequency) happens.

A notification is being considered shown as soon as the extension selects it and before it is being displayed to the user.

## Display methods

| [Type](#notification-types) | [Icon](#toolbar-icon) | [Web notification](#web-notification) | [Popup notification](#popup-notification) | [New tab](#new-tab)
|-|-|-|-|-|
|critical|yes|yes|`#C90000`|no|
|relentless|no|yes|no|no|
|newtab|no|no|no|yes|
|information|yes|no|`#0797E1`|no|
|normal|no|yes|no|no|
|(default)|no|no|`#E6E6E6`|no|

Not all display methods can utilize all attributes:

| Display method | [Type](#notification-types) | Text | [Links](#links) | [Opt-out](#opt-out)[1] |
|-|-|-|-|-|
|[New tab](#new-tab)|no|no|yes[3]|no|
|[Popup notification](#popup-notification)|yes|yes|yes|yes|
|[Toolbar icon](#toolbar-icon)|yes|no|no|no|
|[Web notification](#web-notification)|no|partial[2]|partial[2]|yes|

1. Not shown for [notification types](#notification-types) that are unaffected by the opt-out.
2. Depending on the browser and OS.
3. Only the first link is being used. Links are arbitrary URLs instead of documentation links.

### Toolbar icon
Design example: https://eyeogmbh.invisionapp.com/share/86U64T9ZXFN#/screens/380727596

* Icon animation will change the icon background color and sign:
  * Show red ABP Icon for 400ms.
  * Fade from red to blue icon for 800ms.
  * Show blue bell icon for 800ms.
  * Fade from blue to red icon for 800ms.
  * Show red ABP icon for 400ms.
  * Repeat steps (1) to (5) two more times.
* Animation stops after user opened the Bubble UI
* Animation should take half as long for "critical" notification

Assets: [/res/abp/notifications/](/res/abp/notifications/)

### Web notification

[Web notifications](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API) are windows that appear detached from the browser. Their appearance differs depending on the browser and OS the user is on.

An additional button will be added for each link. The label of the button will contain the content of the corresponding `<a>` tag from the message. If the button is clicked the link will be opened in a new tab.

![](../../res/abp/notifications/notification-link.png)

If the notification contains more links than it can display, it will instead only add a single button whose label will be `Open all links from this notification`. If that button is clicked all links will be opened in new tabs.

![](../../res/abp/notifications/notification-links.png)

Web notifications can be closed by clicking the _X_ icon.

Regardless of how many links the notification contains, it will always reserve one button for [disabling notifications](#opt-out) and label it `Configure notification settings`.

### Popup notification

https://eyeogmbh.invisionapp.com/share/E4TWSPWPTRW

Popup notifications are shown as part of the Bubble UI.

Assets: [/res/abp/notifications/](/res/abp/notifications/)

Popup notifications can be closed by clicking _Close_ which will return the [toolbar icon](#toolbar-icon) and [popup notification](#popup-notification) to their original state.

Clicking on _Stop showing notifications_ will do the same, but also [disables notifications](#opt-out).

### New tab

New tab notifications don't show any content, instead they open the [link](#links) in a new foreground tab the next time the user opens a new tab (inside the same window) that was not the result of navigating to a web page (e.g. clicking a web link that opens in a new tab).

The notification should be marked as seen only once the link has been opened. In case more than one link is defined only the first link is opened the rest are silently ignored, in case no link is defined no link should be opened and the notification can be marked as seen immediately.

The URL can include none, one or more of the following placeholders:

| Placeholder | Description |
|-|-|
|`%LANG%`|Locale code (e.g. `en_US`)|
|`%ADDON_NAME%`|Extension name|
|`%ADDON_VERSION%`|Extension version|
|`%APPLICATION_NAME%`|Browser name|
|`%APPLICATION_VERSION%`|Browser version|
|`%PLATFORM_NAME%`|Browser engine name|
|`%PLATFORM_VERSION%`|Browser engine version|

## Notification content

### Links

Notifications can contain [documentation links](/spec/adblockplus.org/documentation-link.md), see [Links](#links-1) below.

Clicking the link will open the [documentation links](/spec/adblockplus.org/documentation-link.md) in a new tab but not close the notification.

Instead of [documentation links](/spec/adblockplus.org/documentation-link.md), [New tab](#new-tab) notifications can only contain arbitrary URL links.

## Behaviors

### Interval

Notifications can reappear in a given interval which is specified in milliseconds.

Cannot be applied to notifications with type "critical".

### Opt-out

Notifications can be disabled by choosing the respective option in a notification.

This can be changed in the [Customize section](desktop-settings.md#customize-section) on the options page via the _Show useful notifications_ option.

## Targeting options

A notification will not be shown unless all targeting options of a target match.

- Browser UI locales
- Browser version range
- Extension version range
- Number of blocked requests
- OS version range

## Notification sources

Notifications can be added by ABP while it's running. However, it also regularly checks the backend for notifications to show, which are defined in a file called `notification.json`. Since many notifications show up immediately after they have been downloaded, it is important to understand how this works.

#### New users

For new users, notifications get downloaded one minute after the installation.

#### Existing users

For existing users, notifications get downloaded roughly once per day, but it depends. The logic is as follows:

##### Update check frequency

One minute after starting ABP (e.g. the browser), and then every 60 minutes, ABP checks whether it needs to update notifications.

##### Update check logic

    if last update failed and was less than 24 hours ago then
      end check

    if last update was more than 48 hours ago then
    update immediately, schedule next update in 24 hours
    end check

    if last check was more than 24 hours ago then
    delay the next update's scheduled time by the time that passed since the last check
    end check

    if the scheduled time for the next update has passed then
    update immediately, schedule next update in 24 hours
    end check

## Notification repository format

The notifications are stored in the [notification repository](https://hg.adblockplus.org/notifications/).

The notification repository format uses simple key value pairs. `Title` and `Message` are mandatory, the `severity` will default to *information* if omitted.


***NOTE***: The examples use javascript regular expressions do give an idea which values are valid, they don't necessarily define what is correct.

```javascript
/*
severity = normal

title.en-US = Test
message.en-US = Test
*/

severity = /^(information|normal|critical|relentless|newtab)$/
title./^[\w]{2}-[\w]{2}$/ = /^.+$/
message./^[\w]{2}-[\w]{2}$/ = /^.+$/
```
### Attributes

#### Severity

The severity (type) of the notification. Will default to *information* if omitted.

```javascript
// severity = Normal
severity = /^(information|normal|critical|relentless|newtab)$/
```

#### Title

The title of the notification. The title can be specified in multiple locales.
The default locale *en-US* is mandatory and must always be specified.

```javascript
// title.en-US = Title
title./^[\w]{2}-[\w]{2}$/ = /^.+$/
```

#### Message

The message of the notification. The message can be specified in multiple locales.
The default locale *en-US* is mandatory and must always be specified.

```javascript
// message.en-US = message
message./^[\w]{2}-[\w]{2}$/ = /^.+$/
```

#### Inactive

Don't show notification if inactive

```javascript
// inactive = yes
// inactive = no
inactive = /^(yes|no)$/
```

#### Variants

You can specify multiple variants and define the distribution across users
via the `sample` argument. The sample argument defines in percent (0.5 = 50%)
how many users will get this variant of the notification. If the percentages of
the variants don't add up (to 100%), the base notification will be distributed
in the remaining cases.


```javascript
/*
title.en-US = Base
message.en-US = Base

[0]
sample 0.25
title.en-US = Variant0
message.en-US = Variant0
[0]
sample 0.5
title.en-US = Variant1
message.en-US = Variant1
*/

title./^[\w]{2}-[\w]{2}$/ = /^.+$/
message./^[\w]{2}-[\w]{2}$/ = /^.+$/

[\d+]
sample = /^\d+(|\.|\.\d+)$/
title./^[\w]{2}-[\w]{2}$/ = /^.+$/
message./^[\w]{2}-[\w]{2}$/ = /^.+$/
```

#### Interval

Interval in milliseconds. Notification is shown every interval. Note regardless
of interval the notification will not be shown more often than
[update check frequency](#update-check-frequency).

#### Links

Space seperated list of [documentation links](/spec/adblockplus.org/documentation-link.md). For each entry there has to be a corresponding ```<a>``` tag in the message, the ```<a>``` tag will determine which part of the message will be linked (just like a normal ```<a>``` tag). If there are less ```<a>``` tags than links the superfluous links are ignored. If there are more ```<a>``` tags than links the superfluous ```<a>``` tags are ignored.

```javascript
/*
title.en-US = Title
message.en-US = Message with a <a>link</a>
links = chrome_support

title.en-US = Title
message.en-US = Message with a <a>link</a> and another <a>link2</a>
links = chrome_support knownIssuesChrome_filterstorage
*/
links = /([\w-_]+)+/
```

For notifications with the type `newtab`, arbitrary URLs have to be used instead of [documentation links](/spec/adblockplus.org/documentation-link.md). If there are more than one link, the superfluous links are ignored.

```javascript
/*
title.en-US = Title
message.en-US = Message
links = https://example.com/
*/
links = /^https:\/\/.*$/
```

### Filters

It's possible to limit the users the notification will be shown to by using additional filters. A notification will show up if all filters are satisfied.

#### Target

A space seperated list of filters to only show notification to specific users.

Extension refers to the name and version of the browser extension (e.g. adblockplus).
Application refers to the name and version of the browser (e.g. chrome).
Platform refers to the browser platform (e.g. chromium).
Locales show notifications based on the locale of the user
BlockedTotal shows notification based on the total number of ads blocked (*Prefs.blocked_total*).

```javascript
// target = extension=adblockplus extensionVersion=2.3.0.3702
// target = application=chrome applicationVersion<=1.5.0.964
target = /^(extension|application|platform)=\w+ \1Version(=|<=|=>)[\.\d]+$/
// target = locales=en-US
// target = locales=en-US,de-DE
target = /^locales=([\w]{2}-[\W]{2}[,]*)+$/
// blockedTotal=10
// blockedTotal>=10
// blockedTotal<=10
target = /^blockedTotal(=|<=|=>)\d+$/
// target = extension=adblockplus extensionVersion=2.3.0.3702 locales=en-US blockedTotal>=1000
```

If multiple targets are specified only one needs to be (fully) satisfied for the notification to show up.

```javascript
// target = (first AND second)
//                 OR
// target = (third AND forth)
target = application=chrome applicationVersion>=60.0
target = application=firefox applicationVersion>=55.0
```

#### Start

Don't show the notification before `start`.

```javascript
// start = '2017-07-24T12:00'
start = /^[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}$/;
```


#### End

Don't show the notification after `end`.

```javascript
// end = '2017-07-24T12:00'
end = /^[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}$/;
```

## User Journey 

### Day-1

Design: https://eyeogmbh.invisionapp.com/share/VCSAJMYWFMB#/screens

Smaller screen design: https://eyeogmbh.invisionapp.com/share/RBSRI20F725#/screens

Assets: [/res/abp/notifications/day-1/](/res/abp/notifications/day-1/)

*  Day-1 notification should be implemented on Desktop devices only
*  Logo link: [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=adblock_plus` .
*  Day-1 feature can be turned-off as part of [suppress_first_run_page](https://adblockplus.org/en/preferences#suppress_first_run_page)
*  The bubble UI image should be labeled for assistant technologies as `Example of icon pop-up with "Block element" button highlighted.`.

#### Push Notification
* After 30 min and minimum 55 blocked ads, user should get push notification with ABP icon. If ABP didn't block the minimum amount, show the notification once it reached 55. 
* Use Normal type notification
* Click on the notification should open the Day-1 Landing Page

Title: `You've already blocked XX ads!`

Description: `That's a lot of annoying ads. Find out what else Adblock Plus can block.`

#### Landing Page

**Main message:**  `Adblock Plus blocked XX ads in the last half hour, some of which might have contained malicious content`

Button: `Learn more about malicious advertising` [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=adware`

**Secondary message title:** `Join the Adblock Plus community`

Secondary message content: `As an open source project, we depend on users like you to help us improve your ad-blocking experience.`

`What features would you like to see? What can we do better? Is Adblock Plus making some websites misbehave?`

Button: `Contact us!` . Link to: support@adblockplus.org, subject: `Looking for support!`

**Third message title:** `Tired of comment sections bringing you down?`

Third message content: `Block them with Adblock Plus's nifty block element feature.`

Button: `Learn how` [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=block_element`

#### Day-1 Footer

`Copyright © {current year} All rights reserved. Adblock Plus® is a registered  trademark of [eyeo GmbH][1].`

[1] [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=eyeo`

### Warning - Data corruption and Filters Reset

Design: https://eyeogmbh.invisionapp.com/share/NTUPSIHSF92#/screens

Assets: [/res/abp/notifications/data-corruption/](/res/abp/notifications/data-corruption/)

* For Firefox mobile - Open first-run page and show [warning message](first-run.md#warning) instead of notification
* If data was corrupted or filters were restarted, show Popup notification (without icon animation): `An issue has caused your ABP settings to be reset to default. Fix the issue and learn more`
* Notification layout-color  should be Orange `#FF8F00`
* For DE and FR languages use information notification type.
* `Fix the issue and learn more` opens a landing page with the following message:

H1 - `A browser issue has caused your ABP settings to be reset.`

Subheading - `Because of this, your ABP settings have been reset to default. To resolve the issue, you'll need to uninstall and reinstall Adblock Plus. Click the link below to do this.`

Opera Subheading - `Because of this, your ABP settings have been reset to default. To resolve the issue, you'll need to uninstall and reinstall Adblock Plus. Follow the steps below to do this.`

`Note that depending on how you use ABP, you may need to re-configure your settings, custom filters, and whitelisted websites once you reinstall Adblock Plus.`


H2 - `The solution:`

* CTA - `Click here to uninstall and reinstall Adblock Plus`
  * Chrome - [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=chrome_store`
  * Edge - [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=edge_store`
  * Firefox - [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=firefox_store`
  * `open-new-tab-icon.svg` - For RTL languages the new tab icon is mirrored

#### For Opera show the folowing stepts (because their store doesn't allow uninstalling):

Design: https://eyeogmbh.invisionapp.com/share/J6VOYKJBXWN

1. `Open this link to Opera Addons (it will open in a new tab)` [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=opera_store`
2. `From the Opera toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extension**.` Windows and Mac are placeholder for icons
3. `Click the **arrow** next to the Adblock Plus icon.`
4. `Locate Adblock Plus and click the **X** in the upper-right corner.`
5. `Click **Remove**.`
6. `Refresh the Opera Addons page.`
7. `Click **Add to Opera**.`

H3 - `Still looking for help?`

Subheading - `Experiencing an issue? Contact our support team`
* Twitter - [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=social_twitter`, alt-text: `Twitter`
* Facebook - [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=social_facebook`, alt-text: `Facebook`
* Email - support@adblockplus.org, alt-text: `Email`

