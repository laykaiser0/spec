# Updates Page

Updates page is opened via popup notification to announce a new version update (when there is something to inform). 

## Design

* [https://eyeogmbh.invisionapp.com/share/CVVGO1J2FQG#/screens](https://eyeogmbh.invisionapp.com/share/CVVGO1J2FQG#/screens)
* Assets: https://zpl.io/bJ161xK and [res/abp/notifications/update-page/](res/abp/notifications/update-page/)
    * hero image: [/res/abp/notifications/updates_panda.gif](/res/abp/notifications/updates_panda.gif)

## Notification process

* Information type notification; content is defined per version:
    - Notification title: `Adblock Plus is up-to-date`
    - Notification message: `No action is required—continue browsing the web without annoying ads! <a>See what's new</a>`
* Only shown to desktop users
* Only shown to users with locales of the supported core languages
* Clicking on CTA in popup notification opens the updates.html page

## Header content

* ABP logo - [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=adblock_plus in a new tab
* Animation - the animation is different with every version
* Latest ABP version number
* Title and description are different with every version
    * Title: `Adblock Plus is up-to-date!`
    * Description: `We think you'll like the latest updates.`

## Main content

* Information content varies for each release and includes one or both of the following topics:
    * Improvements
    * Fixes
* Each topic includes one or more short snippet updates, unless there is nothing noteworthy to mention
* Some snippets may include animated GIFs

### Current updates

* Improvements:
    * `ABP user interface is now a little more user-friendly`
        * `Added a short description of the Block Element feature and improved wording for the Report button.`
    * `Updated and improved Block Element feature`
        * `We've made the Block Element feature easier to use by adding short instructions as well as a preview of how the page will look without the intrusive element. Try it out!`
        * Image: [/res/abp/notifications/update-page/block_element_update_page.png](/res/abp/notifications/update-page/block_element_update_page.png)
        * Image description: `Example of icon pop-up with redesigned "Block element" button.`

#### Notes for translators

* Overall tone is friendly, short, and to-the-point
* Quick snippets to inform the user of noteworthy updates
* Note about first point: The notification page used to appear in the foreground, now it appears in a background tab

## Contribute

* Title: `Enjoying Adblock Plus?`
* Description: `Consider supporting ABP in one of two ways:`
* `Rate it!`
* `Please take a moment and help spread the word by rating Adblock Plus. It's quick and free!`
* Rating section is not visible for Edge users
* CTA button: `Rate it`
    * CTA button links to the relevant store, if possible to the review page.
    * [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=chrome_review
    * [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=firefox_review
    * [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=opera_review
* `Contribute to the ABP project`
* `Your support allows us to continue to improve Adblock Plus.`
* CTA button: `Contribute`
    * CTA button links to: [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=donation_update_page

## Contact us

* `We want to hear from you!`
* `Share your thoughts on the latest ABP update.`
    * Twitter icon - [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=social_twitter in a new tab , alt text:`Twitter`
    * Facebook icon - [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=social_facebook in a new tab , alt text: `Facebook`
    * Email - support@adblockplus.org  , alt-text:`Email`
* Copyright text `eyeo GmbH` links to [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=eyeo
