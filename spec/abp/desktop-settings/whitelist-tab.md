## Whitelist tab
[Back to top of page](#options-page)

### Whitelist tab empty
![](/res/abp/desktop-settings/whitelisted-websites.jpg)

1. [Whitelist tab headline](#whitelist-tab-headline)
1. [Whitelist tab description](#whitelist-tab-description)
1. [Whitelist textinput](#whitelist-textinput)
1. [Add whitelisted domain button](#add-whitelisted-domain-button)
1. [Add whitelisted domain error message](#add-whitelisted-domain-error-message)
1. [Empty Whitelist placeholder](#empty-whitelist-placeholder)

#### Whitelist tab headline
`Whitelisted websites`

Navigation label will match headline.

#### Whitelist tab description
`You’ve turned off ad blocking on these websites and, therefore, will see ads on them. [Learn more][1]`

[1]: [Documentation link](/spec/abp/prefs.md#documentation-link) *whitelist*

#### Whitelisting a website
##### Behaviour
When a duplicate entry is made, move the original entry to the top of the list and discard the duplicate.

Convert URLs into domains when adding domain to whitelist.

##### Text input 
Text input to enter domains to be added to the whitelist.

##### Label in text input 
`e.g. www.example.com`

#### Add whitelisted domain button
##### Overview
Button to submit [Whitelist text input](#text-input).

If clicked whitelist domain and show [Whitelisted domain added notification](#whitelisted-domain-added-notification).

Refer to [w3 guidelines for keyboard interaction](https://www.w3.org/TR/2016/WD-wai-aria-practices-1.1-20161214/#button)

> A whitelisted domain is an exception rule filter for a certain host.

##### Button label 
`Add website`

#### Add whitelisted domain error message
Shows an error message if [Add whitelisted domain text input](#text-input) doesn't validate.

#### Empty Whitelist placeholder
Placeholder text shown as long as there are no whitelisted domains.

`You don't have any whitelisted websites.`

`Websites you trust and want to allow ads on will be shown here.`

### Whitelist tab populated
![](/res/abp/desktop-settings/whitelisted-websites-populated.jpg)

#### List of whitelisted domains
##### Overview
Displays all whitelisted domains. 

##### Behaviour
List items are sorted alphabetically when loaded.

Move newly added items (from the current session) to the top of the list.

When a duplicate domain entry is made, move the original entry to the top of the list.

<!-- This is to technical, find a better way to describe it -->
***Note***: *All whitelisted domains* does only refer to exception rules that match the following regexp and a member of the *SpecialSubscription* filter list:

```javascript
/^@@\|\|([^\/:]+)\^\$document$/
```

#### Remove whitelisted domain link

Clicking on the bin icon deletes the corresponding domain from the whitelist.

### Whitelisted domain added notification
![](/res/abp/desktop-settings/whitelisted-websites-notification.jpg)

#### Whitelisted domain added notification
##### Overview
The notification should appear as an overlay sliding in from the top of the page.

The notification will disappear if the X is clicked or automatically after 3 seconds.

##### Notification message
`Website has been whitelisted.`

#### Whitelisted Webpage URL from Bubble UI
- When specific webpage URL was whitelisted from Bubble UI, show the entire address without http(s) and www.
- If the URL is too long, truncate with `...` at the end
