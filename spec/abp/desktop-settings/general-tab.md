## General tab
### General tab default
![](/res/abp/desktop-settings/general-tab.jpg)

1. [General tab headline](#general-tab-headline)
1. [General tab description](#general-tab-description)
1. [Privacy & Security section](#privacy-security-section)
1. [Acceptable Ads section](#acceptable-ads-section)
1. [Language section](#language-section)
1. [More filters section](#more-filters-section)
1. [Error state](#error-state)

#### General tab headline
`General`

Navigation label will match headline.

#### General tab description
`Determine what Adblock Plus shows and hides on websites`

### Privacy & Security section
![](/res/abp/desktop-settings/general-default-privacy-and-security.jpg)

1. [Privacy section headline](#privacy-tab-headline)
1. [Recommended filter lists](#recommended-filter-lists)
1. [Tooltip icon](#tooltip-icon)
1. [Tooltip pop-up](#tooltip-pop-up)

#### Privacy section headline
 `PRIVACY & SECURITY`

#### Recommended filter lists
Checkbox to install/uninstall each filter list.

| Filter list name | Filter list title | Tooltip | Filter list URL |
|-----------|---------------|---------------|--------------|
| Fanboy's Annoyances | `Block social media icon tracking` | `The social media icons on the websites you visit allow social media networks to build a profile of you based on your browsing habits - even when you don’t click on them. Hiding these icons can protect your profile.` | https://easylist-downloads.adblockplus.org/fanboy-annoyance.txt |
| EasyPrivacy | `Block additional tracking` | `Protect your privacy from known entities that may track your online activity across websites you visit.` | https://easylist-downloads.adblockplus.org/easyprivacy.txt |

#### Tooltip icon

Tooltip is triggered when users click on the `?` icon, clicking on `X` or outside of the pop-up closes the tooltip.

#### Tooltip pop-up

The tooltip will open in the direction that has the most vertical space available in the current viewport.

The tooltip pop-up should not extend beyond 12.5em in height. If the text overflows beyond this height, then add a scroll bar in.

Refer to the table in [Recommended filter lists](#recommended-filter-lists) for tooltip descriptions. 

### Acceptable Ads section
![](/res/abp/desktop-settings/general-default-acceptable-ads.jpg)

|Section|Content|Behavior|
|-------|-------|--------|
|1. Acceptable Ads section headline|`ACCEPTABLE ADS`| |
|2. Acceptable Ads section description|`Acceptable Ads are nonintrusive ads. They are the middle ground between ad blocking and supporting online content because they generate revenue for website owners.`| |
|3. Acceptable Ads options|`Allow Acceptable Ads` |Checked by default.|
|3a. Acceptable Ads options Description |`Acceptable Ads are not annoying and do not interfere with the content you are viewing. [Read more about the Acceptable Ads criteria][1]` |[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=acceptable_ads_criteria` in a new tab|


#### Acceptable Ads opt-out Survey

**Design:** 
https://scene.zeplin.io/project/5bb73eebd31f1e2e9818f62a/screen/5cc33d52fca2322d1906b80a

*  When user opt-out from Acceptable Ads, tooltip will shown with the following text: `To help us improve Adblock Plus, mind sharing why you’ve turned off Acceptable Ads?`
*  `GO TO THE SURVEY` -  `%LINK%=acceptable_ads_survey` - Open in new tab
*  `NO, THANKS` - Dismiss message. Do NOT dismiss the message if user clicked somewhere else or moed to another Settings section. The message should be dismissed only by clicking on `NO, THANKS`

##### Only allow ads without third-party tracking
This appears as a subset of the `Acceptable Ads` options, which enables the Privacy-friendly Acceptable Ads filter list. 

This option is only active when `Allow Acceptable Ads` is selected, otherwise it will be inactive with reduced opacity.

###### Title:
`Only allow ads without third-party tracking`

###### Description:
`[Learn more][1]`

[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=privacy_friendly_ads` in a new tab.

##### Do not track conditions
![](/res/abp/desktop-settings/general-default-acceptable-ads-dnt.jpg)

If a user selects `Only allow ads without third-party tracking` AND has DNT disabled, display the below text within the table:

`**Note:** You have **Do Not Track (DNT)** disabled in your browser settings. For this feature to work properly, please enable **DNT** in your browser preferences. [Find out how to enable DNT][1] and redirect to the official browser instructions for enabling DNT)`

[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=(adblock_plus_chrome_dnt|adblock_plus_firefox_dnt|adblock_plus_opera_dnt|adblock_plus_edge_dnt)` depending on which browser the extension is running in a new tab.

#### Acceptable Ads notification

![](/res/abp/desktop-settings/general-default-acceptable-ads-message.jpg)

If a user selects `Block additional tracking` (EasyPrivacy) and `Allow Acceptable Ads`, then show the above notification within the page. This will continue showing until the user actively clicks to close the message. 

Clicking X or `OK, got it` closes the notification.

##### Notification text

```
We noticed you have both **Block additional tracking** and **Allow Acceptable Ad** enabled.

We want you to know that in order for advertisers to show you more relevant ads, there *may* be some tracking with Acceptable Ads.

If you prefer extra privacy, select the **Only allow Acceptable Ads that are privacy-friendly** checkbox below.

[OK, got it](closes notification)
```
### Language section
![](/res/abp/desktop-settings/general-default-language.jpg)

1. `LANGUAGE`
1. [Language section description](#language-section-description)
1. [Default language](#default-language)
1. [Change language](#change-language)
1. [Add language](#add-language)
1. `**TIP:** Only select the languages you need. Selecting more will slow down the ad blocker and, therefore, your browsing speed. `


#### Language section description
`Optimize Adblock Plus for the language(s) you typically browse websites in.

#### Default language

Language filter list selected based on the browser's default language. Bundled filter list labels are displayed as follows:

"*language of filter list* + English" [font color: ![#4A4A4A](https://placehold.it/15/4A4A4A/000000?text=+) `#4A4A4A`] "(*Filter list name*)" [font color: ![#BBB](https://placehold.it/15/BBB/000000?text=+) `#BBB`]

If it is an unbundled filter list, then hide `+ English`.

#### Change language
##### Label 
`CHANGE`

##### Behaviour
Button to trigger the [Language dropdown](#language-drop-down).

Clicking on a language within the dropdown will automatically close the window and *change the language filter subscription*. 

#### Add language
##### Label 
`+ ADD A LANGUAGE`
 
##### Behaviour
Button to trigger the [Language dropdown](#language-drop-down).

Clicking on a language within the dropdown will automatically close the window and *add the language filter subscription*. 

### Language drop down
![](/res/abp/desktop-settings/general-default-language-drop-down.jpg)

1. [Drop down](#drop-down)
1. [Selected language](#selected-language)
1. Hover state
1. `SELECT A LANGUAGE` 
1. [Scroll bar](#scroll-bar)

#### Drop down

The bundled language filter subscriptions are always used (i.e. it includes EasyList), unless it is EasyList, which is available unbundled. 

Display labels in the drop down as "*language of filter list* + English", unless it is EasyList, then only display `English`.

Clicking anywhere outside of the drop down, or on a [selected language](#selected-language) closes the drop down.

#### Selected language

Already added filter subscriptions will appear greyed out and disabled in the drop down.

#### Scroll bar

The size of the layover menu should correspond to the screen size. The scroll bar should adjust accordingly. 

#### Multiple languages
![](/res/abp/desktop-settings/general-default-language-multiple.jpg)

##### Behaviour
- When there is more than one language filter list in the table, the  [Change language](#change-language) element turns into a bin icon. All filter lists are removable.
- When there is only one language filter list in the table, the remaining filter list is not removable, and the user is only allowed to  [Change language](#change-language) the filter list - this triggers the [Language modal window](#language-drop-down).

##### Removing a language
To remove a language from the table, click the bin icon in the language row. 

#### Language filter list - empty state
![](/res/abp/desktop-settings/general-tab-language-empty.jpg)

##### Text
`You don't have any language-specific filters.`

##### Conditions
When all language filter lists have been removed from the extension.

Note: it is only possible to remove all language filter lists in [Remove filter list subscriptions](#remove-filter-list-subscriptions) in the Advanced tab.

### More filters section

![](/res/abp/desktop-settings/general-default-more-filters.jpg)

1. [More filters section headline](#language-section-headline)
1. [More filters section description](#language-section-description)
1. [More filter subscriptions](#more-filter-subscriptions)
1. [More filters tooltip](#more-filters-tooltip)
1. [Remove more filters](#remove-more-filters)
1. [More filters note](#more-filters-note)

#### More filters section headline
`More filters`

#### Language section description
`These are additional filters you previously added to Adblock Plus.`

#### More filter subscriptions
All filter lists that a user subscribes to which are not one of Adblock Plus's recommended filter lists (i.e. not recommended within the `Privacy & Security` and `Language` sections) will be shown here. 

#### Remove more filters
You can remove filters by clicking on the `Remove` button.

#### More filters notes

Description `**Note:** You should only use third party filter lists from authors that you trust.`

### Error state

When there is a failure of an action within the page, i.e. if a filter list download fails, display the following error message `Uh oh! Something went wrong. Please try again.`.

This will appear as a banner that will animate from the top down for 3 seconds.

![](/res/abp/desktop-settings/general-tab-error.jpg)
