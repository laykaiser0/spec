# Advanced tab

Design: <https://eyeogmbh.invisionapp.com/share/3GS76ARU5D6>

Title: `Advanced`

Description: `Customize Adblock Plus, add or remove filter lists, create and maintain your own filter lists`

## Spec Sections

1. [Customize section](#customize-section)
1. [Filter lists section](#filter-lists-section)
1. [Custom filters section](#custom-filters-section)


## Customize section

Title: `CUSTOMIZATIONS`

| Behaviour | Text | Tooltip |
|---------------|---------------|---------------|
| Checkbox to en-/disable icon number | `Show number of ads blocked in icon` | n/a |
| Checkbox to en-/disable [Block Element Context Menu Entry](#TBA) | `Show 'Block Element’ right-click menu item` | `Temporarily block annoying items on a webpage, e.g. images or animated slideshows.` |
| Checkbox to en-/disable [Adblock Plus Developer Tools Panel](#TBA) | `Show 'Adblock Plus' panel in developer tools` | `View blocked and whitelisted items from your browser's developer tools panel.` |
| Checkbox to en-/disable [Notifications](#TBA) | `Show useful notifications` | `Allow notifications from Adblock Plus (notifications related to critical performance issues will always be shown).` |

Tooltip behaviour is specified in [Tooltip icon](./general-tab.md#tooltip-icon).

<!-- Opt-in data collection is part of telemetry MR: <https://gitlab.com/eyeo/specs/spec/merge_requests/211> -->

## Filter lists section

Title: `FILTER LISTS`

Description: `Each Adblock Plus setting functions because of a filter list. Below are the filter lists that correspond to all of your Adblock Plus
settings. You can also add additional filters created and maintained by our trusted community. [Learn more][1]`

[1]: [Documentation link](/spec/abp/prefs.md#documentation-link) *subscriptions*

### Filter list columns:

- `Status` - Indicates whether or not the filter list subscription is `Active`/`Disabled`.
  - **Exception:** Status toggle is not shown for Allow nonintrusive advertising and Allow nonintrusive advertising without third-party tracking filter lists.
- `Filter lists` - Displays a filter list's title or its URL, in case the title is missing. In case of long URLs, the word-break is applied to make it fit without overflowing or expanding the cell.
- `Last updated` - Shows when the filter list was last updated, as specified in [Last filter update](#datetime-of-last-filter-list-update).  If there is an issue with downloading or updating a filter list, a relevant error is displayed, as specified in [Error status messages](#error-status-messages) In case of a long error message, the word-break is applied to make it fit without overflowing or expanding the cell.

#### Date/Time of last filter list update

  - Show `Just now` if less than 5 minutes ago
  - Show `minutes ago` if between 5 to 60 minutes ago
  - Show `hours ago` if the last update was between 60 minutes to 24 hours ago
  - Localize dates according to the [Intl.DateTimeFormat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat)
  - Show `Updating` while the list is getting updated
  - All date and time status for filter lists should be updated as filter lists are updated.
  - When there is a a problem with the filter list, display the relevant error message.

#### Error status messages

| Error message | Trigger | Settings status | Settings behaviour |
|-----------|---------------|--------------|------------|
| `Download failed. URL must start with https://.` | URL is not HTTPS | no change | n/a |
| `Download failure` | No HTTP Response or Response is anything other than 2XX | no change | n/a |

#### Settings

Clicking the [gear](/res/abp/desktop-settings/assets/settings.svg) icon opens the following menu:

- `Update now` - Updates the filter list
- `Website` - Links to the homepage of the filter list
- `Source` - Links to the filter list file

The menu should follow [aria best practices](https://www.w3.org/TR/wai-aria-practices-1.1/#menu) for keyboard interactions. Clicking outside the menu closes it. Clicking on a menu item closes the menu.

<!-- This is part of the above menu in some version -->
#### Remove filter list subscriptions

Clicking the [trash.svg](/res/abp/desktop-settings/assets/trash.svg) icon deletes the filter list.

**Exception**: Trash icon is hidden for [Additional subscriptions](/spec/abp/prefs.md#additional-subscriptions) as they can not be removed.

#### Filter list buttons

- `Update all filter lists` - Updates all dates and time statuses for filter lists.
- `+ Add built-in filter lists` - Opens a dropdown for adding predefined filter lists
  - All filter lists are displayed as follows: "*Filter list title*  (*Filter list description*)"
  - Selected and enabled filter lists are marked with a check and have light blue background
  - Selected and disabled filter lists have light grey background
  - [Non-language filter lists](/spec/abp/filter-lists.md#non-language-filter-lists) are displayed on top, while  [Language filter lists](/spec/abp/filter-lists.md#language-filter-lists) are listed below the `LANGUAGE FILTER LISTS` title
  <!-- What about the following three requirements? -->
  - Already installed filter lists are noninteractive
  - Clicking on a not selected filter list closes the dropdown and adds the enabled filter list in the Filter Lists table
  - Clicking the `ADD BUILD-IN FILTER LISTS` while the dropdown is opened, or clicking anywhere outside of the dropdown, closes the dropdown
- `+ Add filter list via URL` - Opens the [Add filter list via URL popover](#add-filter-list-via-url)
    - `ADD FILTER LIST VIA URL` button adds a valid URL to the table and closes the popover, or shows error message `Enter a valid URL` for invalid URL while briefly flashing the warning message while keeping the popover open
    - `Cancel` closes the popover without discarding the content of the input field
    <!-- What about the following three requirements? -->
    - Input field validates as is typed against: `/^https?:/i`
    - As soon as the input is validated, the warning disappears
    - Tab focus on the `ADD A FILTER LIST` button before `CANCEL`


### Add filter subscription dialogue

Design: https://eyeogmbh.invisionapp.com/share/3GS76ARU5D6#/screens/365281395

##### Overview

| Element | Content |
|---------------|---------------|
| Dialog title | `Are you sure you want to add this filter list?` |
| Warning | `Enter a valid URL` |
| `Title` | `<Filter list title>` (if a title exists, otherwise hide the title section) |
| `URL` | `<Filter list URL>` |
| Button | `Yes, add this filter list` |

- Dialog is opened when a user adds a filter list by clicking on a subscribe link from an external source.
- Clicking `Yes, add this filter list` button adds the filter list. 
- Clicking on the `X` in the upper right corner closes the dialog without adding the filter list.

Users following a non-HTTPS subscribe link are shown an [error screen](https://eyeogmbh.invisionapp.com/share/3GS76ARU5D6#/screens/365281395)
   - Message title: `ERROR OCCURRED`
   - Message body: `For security reasons, only HTTPS filter lists and data URLs can be added.`
   - `Cancel`

#### Filter list table - empty state

When all filter lists have been removed from the extension, a message: `You have not added any filter lists to Adblock Plus. Filter lists you add will be shown here.` is shown.

### Custom filters section

Design: https://eyeogmbh.invisionapp.com/share/XGT31Y9H3DP#/screens

1. Section title: `YOUR CUSTOM FILTERS`
1. Description: `Create and maintain your own filters to further control what content Adblock Plus allows or blocks. [Learn how to write filters (English only)](%LINK%=filterdoc)`
1. Table title: `My filter list`
1. [Input field](#input-field)
1. [Add button](#add-button)
1. [Checkbox](#checkbox)
1. [Filter rule toggle](#filter-rule-toggle)
1. [Filter rule](#filter-rule)
1. [Alert](#alert)

#### Input field

Instructional prompt in the text box reads `Search or add filter(s) (e.g. /ads/track/*) `.

##### Searching and adding filters:

After at least 3 characters are entered, the table automatically starts selecting filter rules and scrolling to the first item in the filter list that matches the entered pattern. This check is performed after each keystroke.

 - If an exact match is found, the [+ ADD button](#add-button) is disabled, to prevent adding a duplicate.
 - If no matches, or multiple matches are found, the [+ ADD button](#add-button) is active and the unique filter rule can be added to the table via pressing [+ ADD button](#add-button) or `enter key`. After adding a filter, the table scrolls to the top and new filter is added as a new row to the top.

The input field supports adding multiple filters. Multiple filters are automatically added by pasting multiline filters directly into the input field. After pasting, the input field remains empty, table scrolls to the top and filters are added to the top of the table.

###### Exceptions for adding a single filter:

 - If an invalid filter of type error, as defined in the [Alert](#alert) section, is to be added to the table, its corresponding error is shown in the table's footer, filter is not added, and entered filter pattern remains in the input field.
 - If a filter list header is to be added, it gets silently ignored. There are no errors shown and the input field is cleared after clicking the [+ ADD button](#add-button) or `enter key`.

###### Exceptions for adding multiple filters:

 - If pasted filters contain a duplicated filter, the existing filter is replaced by the pasted one.
 - If pasted filters contain a filter list header, it is silently ignored and only valid filters get added to the table.
 - If pasted filters contain an invalid filter of type error, as defined in the [Alert](#alert) section, its corresponding error, together with the line number is shown in the table's footer. None of the pasted filters get added to the table and the input field contains all pasted filters in one single line.

#### Add button

The `+ADD` button is disabled if the input field is empty, or the pattern entered matches exactly one filter in the filter list. Clicking `+ADD` button scrolls the table to the top an adds the filter rule as a new row at the top of the list.

#### Checkbox

Checkbox for selecting filters. Checking the checkbox in the column header selects, while unchecking it deselects all filters in the filter list. Column header changes to a darker shade on hover to indicate that it is interactive.

Checking/unchecking the checkbox next to an individual filter will select/deselect that filter. Selected filter's row in the table changes to a darker shade to indicate that the filter is selected.

When at least one filter is selected, the `Delete` and `Copy selected` buttons are shown to delete and copy the selected filters respectively, as shown below. When no filters are selected the buttons are hidden. After copying selected filters, the filters remain selected and the buttons visible. After deleting selected filters, no filters are selected and the buttons are hidden.

![](/res/abp/desktop-settings/custom-filter-multiple-selected.jpg)

#### Filter rule toggle

Toggle to enable/disable individual filters. Toggle is hidden for comments.

Clicking on the column header sorts the filter list so that enabled filters are on top of the table, while disabled filters are on the bottom. Clicking it again reverses the order. Column header changes to a darker shade on hover to indicate that it is interactive.

#### Filter rule

Sortable list of filter rules. Clicking on the column header sorts the list alphabetically, clicking the header again reverses the order. Column header changes to a darker shade on hover to indicate that it is interactive. Within a session, filters should be ordered with the last added filters at the top of the table.

##### Editing filters:

Hovering a filter shows dashed border around it. A filter can be edited by clicking in its Filter rule field. In case of long filters, the field expands. Changes are saved by pressing `enter key` or clicking outside of the filter field. If the edited filter is valid after saving, the Filter rule field briefly flashes green.

Edited filters stay where they are in the table and are not moved to the top.

###### Special editing cases:

 - Editing a filter into a comment hides the [Filter rule toggle](#filter-rule-toggle) and vice versa.
 - Editing a filter into a filter of type warning, as defined in the [Alert](#alert) section, makes a grey alert icon appear in the Alert column and vice versa. Hovering on the icon shows filter's corresponding warning message.
 - Editing a filter into an invalid filter of type error, as defined in the [Alert](#alert) section, makes a red alert icon appear in the Alert column. Hovering on the icon shows invalid filter's corresponding error message. Changes are not persistent and the filter is reverted back to its valid state on refresh.

#### Custom Filters Alert

Alerts are displayed for erroneous and slow performing custom filters.

**Warning alerts** are shown for slow performing filters. Such filters are still considered valid, and can therefore be added to the custom filters table. Warning alerts are indicated by a grey warning icon, that is displayed next to the slow performing filter.  A tooltip on hover is shown, containing the message as defined in the table below.

**Error alerts** are shown for erroneous filters. Such filters are considered invalid and can not be added to the filter table, but an existing filter can be edited into an invalid state. Error alerts are indicated by a red warning icon, that is displayed next to the erroneous filter. A tooltip on hover is shown, containing the message as defined in the table below.

| Error states | Messages | Type |
| --- | --- | --- |
| elemhideemulation nodomain | `No active domain specified for extended element hiding filter` | error |
| invalid csp | `Invalid Content Security Policy` | error |
| invalid domain | `Invalid (or empty) domain specified` | error |
| invalid regexp | `Invalid regular expression` | error |
| snippet nodomain | `No active domain specified for snippet filter.` | error |
| unexpected filter list header | `Filter list headers aren't allowed here` | error
| unknown option | `Unknown filter option` | error |
| short pattern | `To ensure a fast filter, please check the length of the pattern and ensure it doesn't contain a regular expression.` | warning |
| filter action failed | `Something went wrong. Please try again.` | error |

#### Empty state
Design: https://eyeogmbh.invisionapp.com/share/XET31YGWDUN#/screens

Empty state is shown when the custom filters table is empty.

Instructional prompt in the text box reads `Search or add filter(s) (e.g. /ads/track/*)`.
